FROM openjdk:8-alpine

COPY target/uberjar/browser-page-switcher.jar /browser-page-switcher/app.jar

EXPOSE 3000

CMD ["java", "-jar", "/browser-page-switcher/app.jar"]
