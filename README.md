# browser-page-switcher

## install
`lein deps`

## run server
`lein run`

## dashboard
 http://127.0.0.1:3000/index.html

## swagger
 http://127.0.0.1:3000/api/api-docs/index.html
