(ns browser-page-switcher.url-producer)

(def url-list
  '("https://timesofindia.indiatimes.com/india"
    "https://commodore-news.com/news/index/1/en/desktop"
    "http://rsi.untergrund.net/"
    "https://www.helagotland.se/start/"
    "https://www.amiga-news.de/en/"))

(defn get-url []
  (rand-nth url-list))
