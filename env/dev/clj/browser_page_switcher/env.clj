(ns browser-page-switcher.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [browser-page-switcher.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[browser-page-switcher started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[browser-page-switcher has shut down successfully]=-"))
   :middleware wrap-dev})
