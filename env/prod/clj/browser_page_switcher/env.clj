(ns browser-page-switcher.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[browser-page-switcher started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[browser-page-switcher has shut down successfully]=-"))
   :middleware identity})
